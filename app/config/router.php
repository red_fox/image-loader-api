<?php

$router = $di->getRouter(false);

/*
 * API
 */
$router->addPost("/api/uploadImage/", array(
    'controller'    => 'api',
    'action'        => 'uploadImage',
))->setName("apiUploadImage");

$router->addPost("/api/getStatus/", array(
    'controller'    => 'api',
    'action'        => 'getStatus',
))->setName("apiGetStatus");


$router->addGet("/", array(
    'controller'    => 'index',
    'action'        => 'index',
))->setName("index");

$router->handle();
