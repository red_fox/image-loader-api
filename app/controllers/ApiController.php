<?php

class ApiController extends ControllerBase
{
    /*
     * Загрузка картинки
     */
    public function uploadImageAction()
    {
        $this->view->setRenderLevel(
            Phalcon\Mvc\View::LEVEL_LAYOUT
        );

        $options = [
            'upload_dir'               => '../public/files/',
            'accept_file_types'        => '/\.(jpe?g)$/i',
            'correct_image_extensions' => true,
            'image_versions'           => [],
        ];

        $uploadHandler = new UploadHandler($options);

        $imagesArray = array_shift($uploadHandler->response);
        $image       = array_shift($imagesArray);

        if (empty($image->error)) {

            $modelImages                = new Images();
            $modelImages->original_name = $image->name;
            $modelImages->original_size = $image->size;
            $modelImages->status        = 1;

            if ($modelImages->save()) {

                $image->id             = $modelImages->id;
                $image->calculatedTime = $this->approximateCalculation($image);
            } else {

                return;
            }
        }

        $result['files'][] = $image;

        return json_encode($result);
    }

    /*
     * Статус обработки картинки
     */
    public function getStatusAction()
    {
        $imagePath = '/files/processed/';

        $result = [
            'success' => false,
        ];

        $imageId = $this->request->getPost('imageId');

        $modelImages = Images::findFirst([
            'conditions' => 'id = :imageId: AND status = 3',
            'bind' => [
                'imageId' => $imageId,
            ],
        ]);

        if ($modelImages) {
            $result['success'] = true;
            $result['link']    = $imagePath . $imageId . '.jpg';
        }

        return json_encode($result);
    }

    /*
     * Метод расчёта примерного времени загрузки картинки
     */
    public function approximateCalculation($imageObj)
    {
        $threads = $this->config->daemon->threads;

        // Выбираем последние уже обработанные картинки
        // для того чтобы узнать среднее время расчёта
        $AlreadyProcessed = Images::find([
            'conditions' => 'status = 3',
            'limit'      => 10,
            'order'      => 'id DESC'
        ]);

        // Узнаём общий объём и время, затраченное на обработку
        // уже загруженных файлов
        $totalSize = 0;
        $totalTime = 0;
        foreach ($AlreadyProcessed as $image) {
            $totalSize += $image->original_size;
            $totalTime += abs(strtotime($image->start_processing)-strtotime($image->done_processing));
        }

        // Скорость обработки одного потока
        $bps = round($totalSize/$totalTime);

        // Выбираем картинки в очереди на обработку
        $pendingProcessing = Images::find([
            'conditions' => 'status = 1 AND id <= :imageId:',
            'bind'       => [
                'imageId' => $imageObj->id,
            ],
        ]);

        // Выбираем картинки, находящиеся в обработке
        $inProcessing = Images::find([
            'conditions' => 'status = 2 AND id <= :imageId:',
            'bind'       => [
                'imageId' => $imageObj->id,
            ],
        ]);

        $totalFilesToProcessing = $pendingProcessing->count();

        // Если есть ещё свободные потоки на обработку
        $totalFilesInProcessing = $inProcessing->count();
        if ($threads-$totalFilesInProcessing >= $totalFilesToProcessing) {

            $totalSizePending = $imageObj->size;
            $totalFilesToProcessing = 1;
        } else {

            // Узнаём общий объём
            $totalSizePending = 0;
            foreach ($pendingProcessing as $image) {
                $totalSizePending += $image->original_size;
            }
            foreach ($inProcessing as $image) {
                $totalSizePending += $image->original_size;
            }
        }

        // Если файлов на обработку меньше чем потоков
        if ($totalFilesToProcessing <= $threads) {

            $time = $totalSizePending/$bps/$totalFilesToProcessing;
        } else {

            $time = $totalSizePending/$bps/$threads;
        }

        // Тут можно привести время к нужному формату
        return ceil($time) . ' секунд';
    }
}
