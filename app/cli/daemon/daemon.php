<?php

require '../cli.php';

// Пути до папок с картинками
$originalImagesDir    = '../../../public/files/';
$destinationImagesDir = '../../../public/files/processed/';
$threads              = $di->get('config')->daemon->threads;

global $pids, $images;
$pids   = [];
$images = [];

print 'Parent : ' . getmypid() . "\n";

// Демонизируем процесс (форкаемся и убиваем родителя, запущенного из консоли)
$pid = pcntl_fork();

if ($pid) {
    // Только родитель знает pid ребенка
    print 'Parent : ' . getmypid() . ' exiting' . "\n";
    exit();
}

$daemonPid = getmypid();
print 'Daemon : ' . $daemonPid . "\n";

// Задаем обработчик сигналов
declare(ticks = 1) ;

pcntl_signal(SIGTERM, 'sigHandler');
pcntl_signal(SIGHUP, 'sigHandler');
pcntl_signal(SIGINT, 'sigHandler');
pcntl_signal(SIGUSR1, 'sigHandler');

// Указываем воркер для запуска (нужен абсолютный путь, процессы выполняются мимо shell`а и не ориентируются в пространстве)
$programEnd = array(">/dev/null", "2>&1");

// Определяем путь к интерпретатору PHP (по той же причине, что и для воркера)
$phpPath = system('which php');
if (!$phpPath) {
    die ('Ошибка определения местоположения интерпретатора PHP');
}

// Запускаем "бесконечный процесс" для демона - нужен для контроля дочерних процессов, как только все они отработают - выполняем финальные действия и отлючаемся
while (TRUE) {

    // Смотрим, есть ли картинки на обработку
    $imagesCount = $di->get('db')->fetchColumn('SELECT COUNT(*) FROM `images` WHERE `status` = 1');

    // Условие для форка
    if ($imagesCount > 0) {

        $needToBegin = false;

        // Не более 10 потомков
        if (count($pids) < $threads) {

            $startProcessing = date('Y-m-d H:i:s');

            // Берём картинку в работу
            $image = $di->get('db')->fetchOne('SELECT `id`, `original_name` FROM `images` WHERE `status` = 1 FOR UPDATE');

            // Ставим статус "в обработке", чтобы другие потоки не хватали эту запись
            $queryForUpdate = 'UPDATE `images` SET `status` = 2, ' .
                '`start_processing` = \'' . $startProcessing . '\' WHERE `id` = ' . $image['id'];
            $di->get('db')->execute($queryForUpdate);

            $pid = pcntl_fork();
            $needToBegin = true;
        }
        
        if ($needToBegin) {
            if (!$pid) {

                $originalImagePath    = $originalImagesDir . $image['original_name'];
                $destinationImagePath = $destinationImagesDir . $image['id'] . '.jpg';

                // Дочерний процесс - запускаем специально обученного воркера
                $dataForWorker = array(__DIR__ . DIRECTORY_SEPARATOR  . 'tasks' . DIRECTORY_SEPARATOR . 'task.php', $originalImagePath, $destinationImagePath);
                pcntl_exec($phpPath, array_merge($dataForWorker, $programEnd));
                exit();
            } else {
                // Родитель - добавляем дочерний процесс в общий список
                $pids[] = $pid;

                // Запоминаем id картинки и привязываем его к потоку
                $images[$pid] = $image['id'];
            }
        }
    }

    // Собираем все дочерние процессы, которые скончались сами - зомби должны быть упокоены
    $deadAndGone = pcntl_waitpid(-1, $status, WNOHANG);
    while ($deadAndGone > 0) {

        $doneProcessing = date('Y-m-d H:i:s');

        $queryForUpdate = 'UPDATE `images` SET `status` = 3, ' .
            '`done_processing` = \'' . $doneProcessing . '\' WHERE `id` = ' . $images[$deadAndGone];
        $di->get('db')->execute($queryForUpdate);

        unset($images[$deadAndGone]);

        // Убираем pid зомби из массива
        unset($pids[array_search($deadAndGone, $pids)]);

        // Ищем еще зомби
        $deadAndGone = pcntl_waitpid(-1, $status, WNOHANG);
    }

    // Если отработали все извлекающие файлы процессы - значит, миссия выполнена
    /*if (!count($pids)) {

        print 'Daemon : ' . getmypid() . ' работа выполнена' . "\n";
        posix_kill($daemonPid, SIGKILL);
    }*/

    // Пауза в цикле демона
    //sleep(1);
}

// Обработчик для сигналов
function sigHandler($signo)
{
    global $pids;
    if ($signo == SIGTERM || $signo == SIGHUP || $signo == SIGINT) {
        
        // Если родитель перезапускается или убит - посылаем им сигнал KILL
        foreach ($pids as $p) {
            posix_kill($p, $signo);
        }
        
        // Ждем, пока дочерние процессы передохнут
        foreach ($pids as $p) {
            pcntl_waitpid($p, $status);
        }
        
        print 'Daemon : ' . getmypid() . ' все потоки отработали, выключаюсь' . "\n";
        exit();
    } else if ($signo == SIGUSR1) {
        print 'У меня уже ' . count($pids) . ' поток' . "\n";
    }
}
