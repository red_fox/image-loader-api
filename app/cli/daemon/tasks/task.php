<?php
// Файлик имитации обработки картинки

if ($argc > 0) {

    $originalImagePath    = $argv[1];
    $destinationImagePath = $argv[2];

    if (!copy($originalImagePath, $destinationImagePath)) {
        echo 'Не удалось скопировать' . "\n";
    }
    // Выставляем задержку исходя из размера файла
    $delay = ceil(filesize($originalImagePath)/100000);
    sleep($delay);

} else {

    echo 'Что-то не так в работе, конец' . "\n";
}
